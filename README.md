Русский ![][ruflag]
===================
Зачем?
------
Этот небольшой проект преследует несколько целей:

* отработка навыков ООП c использованием С++
* облегчение работы с матрицами
* демонстрация примеров моего кода (для работодателей)

Как что работает?
-----------------
Матрица - это шаблонный класс, параметром к которому является тип элемента. Доступ к элементам матрицы осуществляется тремя способами:

* оператор `(x, y)`
* методы `getElem(x, y)` и `setElem(x, y, ... )`

В остальном, класс предлагает обширный набор методов и операторов для удобной работы с матрицами, а также набор исключений для обработки большинства внештатных ситуаций.

Вклад и сотрудничество
----------------------
Мелкие исправления - pull request.  
Пожелания и предложения - serafimati@yandex.ru.

English ![][engflag]
====================
What for?
---------
This small project has several objectives:

* to improve my OOP skills using C++
* to simplify matrix calculations
* to demonstrate the headhunters samples of my code

How is it made?
---------------
Matrix - is a template class parametrized by its elements type. Three different ways to get access to the matrix element are available:

* `(x, y)` operator
* `getElem(x, y)` и `setElem(x, y, ... )` methods

Also, class offers a wide range of methods and operators which simplify working with matrixes, and a set of exceptions for habdling most of emergency situations.

Contribution and cooperation
----------------------------
You can use pull requests to fix small bugs, or use email: serafimati@yandex.ru to express your comments and suggestions.

[ruflag]:https://twemoji.maxcdn.com/36x36/1f1f7-1f1fa.png
[engflag]:https://twemoji.maxcdn.com/36x36/1f1ec-1f1e7.png
