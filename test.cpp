#include <cassert>  // assert
#include <iostream>
#include <sstream>  // stringstream
#include <vector> // transpose::toVector

#define private public

#include "matrixlib.h"

// operator <<
// UNDONE: Нету проверок для матриц с несколькими строками.
void output_is_valid() {
  std::stringstream stream;  // Arrange
  matrix<int> m(1, 3);       //
  matrix<int> element(1, 1); //

  stream << element;           // Act
  assert(stream.str() == "0"); // Assert

  stream.seekp(0);                 // Arrange
  stream << m;                     // Act
  assert(stream.str() == "0 0 0"); // Assert

  stream.seekp(0);                     // Arrange
  m(0, 0) = m(0, 1) = m(0, 2) = 42;    //
  stream << m;                         // Act
  assert(stream.str() == "42 42 42");  // Assert
}

void opAssign_has_no_allocation() {
  // Arrange
  matrix<int> five(5, 5);
  matrix<int> another_five(5, 5);
  matrix<int> four(4, 4);
  auto saved_five_ptr = five.data;

  auto assign = [](auto& lhs, auto& rhs, size_t count) {
    for (size_t _ = 0; _ < count; ++_)
      lhs = rhs;
  };

  assign(five, another_five, 1'000'000); // Act
  assert(saved_five_ptr == five.data);   // Assert

  assign(five, four, 1'000'000);       // Act
  assert(saved_five_ptr == five.data); // Assert
}

void transpose() {
  using Value_type = double; // matrix<T>::value_type

  auto iotaMatrix = [](size_t rows, size_t cols) {
	auto mx = matrix<Value_type>(rows, cols);

    auto add = Value_type(0);
    for (size_t row = 0; row < rows; ++row)
      for (size_t col = 0; col < cols; ++col)
        mx(row, col) = add++;

    return mx;
  };

  auto toVector = [](const auto& mx) {
    std::vector<std::vector<Value_type>> dump(mx.getRows());
      for (size_t row = 0; row < mx.getRows(); ++row)
        for (size_t col = 0; col < mx.getCols(); ++col)
          dump[row].push_back(mx(row, col));

    return dump;
  };

  auto twoThree = iotaMatrix(2, 3); // { {0, 1, 2}, {3, 4, 5} }

  // act
  twoThree.transpose(); // { {0, 3}, {1, 4}, {2, 5} }

  // assert
  assert( twoThree.getRows()==3 && twoThree.getCols()==2 );

  auto threeTwo = toVector(twoThree);

  // assert
  assert(threeTwo == (std::vector<std::vector<Value_type>>{ {0, 3}, {1, 4}, {2, 5} }));
}

int main() {
  output_is_valid();
  opAssign_has_no_allocation();
  transpose();
}
