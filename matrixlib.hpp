#pragma once
#include <algorithm>
#include <ostream>
#include <stdexcept>

template <class T> class matrix{
private:
	size_t rows, cols;
	T *data;

public:
	/* Constructors */
	explicit matrix(const size_t rows, const size_t cols);
	template <class A>
	explicit matrix(const size_t rows, const size_t cols, A *func(size_t, size_t));
	matrix(const matrix<T> &copy);

	/* Getters */
	inline const size_t getRows() const noexcept;
	inline const size_t getCols() const noexcept;
	const T getElem(const size_t x, const size_t y) const;

	/* Setters */
	void setElem(const size_t x, const size_t y, const T elem);

	/* Destructor */
	~matrix();

	/* Methods */
	void transpose();
	inline bool isSquare() const noexcept;
	const T getTrace() const;

	/* Operators */
	const T operator ()(const size_t x, const size_t y) const;
	T& operator ()(const size_t x, const size_t y);

	const matrix<T>& operator =(const matrix<T> &right);

	matrix<T>& operator *=(const matrix<T> &right);
	template <class A>
	matrix<T>& operator *=(const A right) noexcept;
	matrix<T>& operator +=(const matrix<T> &right);
	matrix<T>& operator -=(const matrix<T> &right);
};