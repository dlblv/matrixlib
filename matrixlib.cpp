#include <matrixlib.hpp>

/*Constructors*/
template <class T>
matrix<T>::matrix(const size_t rows, const size_t cols):
	rows(rows),
	cols(cols){

#ifdef DEBUG
	std::cout << "Constructor (size_t, size_t) called\n";
#endif

	if (rows < 1 || cols < 1)
		throw std::invalid_argument("cols or rows < 1");

	data = new T[rows*cols];
	for (size_t i = 0; i < rows; ++i)
		for (size_t j= 0; j < cols; ++j)
			data[i*cols + j] = static_cast<T>(0);
}

template <class T>
template <class A>
matrix<T>::matrix(const size_t rows, const size_t cols, A *func(size_t, size_t)):
	rows(rows),
	cols(cols){

#ifdef DEBUG
	std::cout << "Constructor (size_t, size_t, func) called\n";
#endif

	if ((rows < 1) || (cols < 1))
		throw std::invalid_argument("cols or rows < 1");

	data = new T[rows*cols];
	for (size_t i = 0; i < rows; ++i)
		for (size_t j = 0; j < cols; ++j)
			data[i*cols + j] = static_cast<T>(func(i,j));
}


template <class T>
matrix<T>::matrix(const matrix<T> &copy):
	rows(copy.rows),
	cols(copy.cols){

#ifdef DEBUG
	std::cout << "Copy constuctor called\n";
#endif

	data = new T[rows*cols];
	for (size_t i = 0; i < rows; ++i)
		for (size_t j = 0; j < cols; ++j)
		data[i*cols + j] = copy.data[i*cols + j];
}

/* Getters */
template <class T>
inline const size_t matrix<T>::getRows() const noexcept{
	return rows;
}

template <class T>
inline const size_t matrix<T>::getCols() const noexcept{
	return cols;
}

template <class T>
const T matrix<T>::getElem(const size_t x, const size_t y) const{
	if ((x >= 0) && (x < rows) && (y >= 0) && (y < cols))
		return data[x*cols + y];

	throw std::out_of_range("x or y is out of range");
}

/* Setters */
template <class T>
void matrix<T>::setElem(const size_t x, const size_t y, const T elem){
	if ((x >= 0) && (x < rows) && (y >= 0) && (y < cols))
		data[x*cols + y] = elem;
	else
		throw std::out_of_range("x or y is out of range");
}

/* Destructor */
template <class T>
matrix<T>::~matrix(){
#ifdef DEBUG
	std::cout << "Destructor called!\n";
#endif
	delete [] data;
}

/* Methods */
template <class T>
void matrix<T>::transpose(){
	std::swap(rows,cols);
	// TODO =================================
}

template <class T>
inline bool matrix<T>::isSquare() const noexcept{
	return (cols == rows);
}

template <class T>
const T matrix<T>::getTrace() const{
	if (this->isSquare()){
		T trace = static_cast<T>(0);
		for (size_t i = 0; i < cols; ++i)
			trace += data[i*cols + i];

		return trace;
	}

	throw std::logic_error("not applicable");
}

/* Operators */
template <class T>
const T matrix<T>::operator ()(const size_t x, const size_t y) const{
	if ((x >= 0) && (x < rows) && (y >= 0) && (y < cols))
		return data[x*cols + y];

	throw std::out_of_range("x or y is out of range");
}

template <class T>
T& matrix<T>::operator ()(const size_t x, const size_t y){
	if ((x >= 0) && (x < rows) && (y >= 0) && (y < cols))
			return data[x*cols + y];

	throw std::out_of_range("x or y is out of range");
}

template <class T>
const matrix<T>& matrix<T>::operator =(const matrix<T> &right){
	if (this != &right){
		rows = right.rows;
		cols = right.cols;
		delete [] data;
		data = new T[rows*cols];
		for (size_t i = 0; i < rows; ++i)
			for (size_t j = 0; j < cols; ++j)
			data[i*cols + j] = right.data[i*cols + j];

	}
	return *this;
}

template <class T>
const matrix<T> operator *(const matrix<T> &left, const matrix<T> &right){
	return matrix<T>(left) *= right;
}

template <class T, class A>
const matrix<T> operator *(const matrix<T> &left, const A right) noexcept{
	return matrix<T>(left) *= right;
}

template <class T, class A>
const matrix<T> operator *(const A left, const matrix<T> &right) noexcept{
	return matrix<T>(right) *= left;
}

template <class T>
const matrix<T> operator +(const matrix<T> &left, const matrix<T> &right) {
	return matrix<T>(left) += right;
}

template <class T>
const matrix<T> operator -(const matrix<T> &left, const matrix<T> &right){
	return matrix<T>(left) -= right;
}

template <class T>
matrix<T>& matrix<T>::operator *=(const matrix<T> &right){
	if(cols == right.rows){
		matrix<T> result(rows, right.cols);
		for (size_t i = 0; i < rows; ++i)
			for (size_t j = 0; j < right.cols; ++j){
				result(i, j) = static_cast<T>(0);

				for (size_t k = 0; k < cols; ++k)
					result(i, j) += (*this)(i, k) * right(k, j);
			}
		*this = result;
		return *this;
	}

	throw std::invalid_argument("");
}

template <class T>
template <class A>
matrix<T>& matrix<T>::operator *=(const A right) noexcept{
	for (size_t i = 0; i < rows; ++i)
		for (size_t j = 0; j < cols; ++j)
			(*this)(i, j) *= static_cast<T>(right);

	return *this;
}

template <class T>
matrix<T>& matrix<T>::operator +=(const matrix<T> &right){
	if ((cols == right.cols) && (rows == right.rows)){
		for (size_t i = 0; i < rows; ++i)
			for (size_t j = 0; j < cols; ++j)
				(*this)(i, j) += right(i, j);
		return *this;
	}

	throw std::invalid_argument("");
}

template <class T>
matrix<T>& matrix<T>::operator -=(const matrix<T> &right){
	if ((cols == right.cols) && (rows == right.rows)){
		for (size_t i = 0; i < rows; ++i)
			for (size_t j = 0; j < cols; ++j)
				(*this)(i, j) -= right(i, j);
		return *this;
	}

	throw std::invalid_argument("");
}


template <class T>
std::ostream& operator << (std::ostream &out, const matrix<T> &matrix){
	const size_t rows = matrix.getRows(),
				 cols = matrix.getCols();

	for (size_t i = 0; i < rows; ++i){
		for (size_t j = 0; j < cols; ++j)
			out << matrix(i, j) << ( (j < (cols - 1) ) ? " " : "" );

		out <<  ( (i < (rows - 1) ) ? "\n" : "" );
	}

	return out;
}