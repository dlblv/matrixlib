CC = clang++
CCFLAGS = --std=c++17 -I.

all: lib

lib:
	$(CC) $(CCFLAGS) -c matrixlib.cpp

test:
	$(CC) $(CCFLAGS) test.cpp -o test

run: test
	./test

clean:
	rm -rf *.o test

.PHONY: test
